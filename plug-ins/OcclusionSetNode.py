import sys, os
import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx
import OcclusionInVertexColor as ovc
reload(ovc)
# node name
kPluginNodeTypeName = "occlusionSetNode"

# Need be a unique ID. Maya does not accept more than one node with the same id
# ... anyway. It's a temporary ID
occSetNodeId = om.MTypeId(0x00000004)

# Attribute names
tinyWorldMatrixLong = 'tinyWorldMatrix'
tinyWorldMatrixShort = 'twm'
tinyMeshLong = 'tinyMeshInput'
tinyMeshShort = 'tmi'
baseMeshLong = 'baseMeshInput'
baseMeshShort = 'bmi'
baseMeshMatrixLong = 'baseMeshMatrix'
baseMeshMatrixShort = 'bmm'
radiusValueLong = 'radius'
radiusValueShort = 'rds'
smoothValueLong = 'smooth'
smoothValueShort = 'smt'
textureLong = 'texture'
textureShort = 'txr'
weightTextureLong = 'weightTexture'
weightTextureShort = 'wt'
statusPreviewLong = 'statusPreview'
statusPreviewShort = 'spw'
annoyingTinyOutputLong = 'annoyingTinyOutput'
annoyingTinyOutputShort = 'ato'
annoyingBaseMeshOutputLong = 'annoyingBaseMeshOutput'
annoyingBaseMeshOutputShort = 'abo'



class OcclusionSetNode(ompx.MPxObjectSet):
    """
    ObjectSet customized.

    This class receives models to calculate its occlusion based on the baseMesh

    Attributes
    -----------
    tinyMatrixIn : MObject (Input)
    baseMeshInput : MObject (Input)
    baseMeshMatrixInput : MObject (Input)
    radiusValueInput : MObject(Input)
    smooothValueInput : MObject (Input)
    textureInput : MObject (Input)
    weightTextureInput = MObject (Input)
    annoyingTinyOutput : MObject (Output)
    annoyingBaseOutput : MObject (Output)
    callbackArray : MCallbackIdArray
        Save callback ids. To use during save, open, and import stage.
    permissionStatus : bool
        Stops processing according to the current callback.

    """

    tinyMatrixIn = om.MObject()
    tinyMeshIn = om.MObject()
    baseMeshInput = om.MObject()
    baseMeshMatrixInput = om.MObject()
    radiusValueInput = om.MObject()
    smoothValueInput = om.MObject()
    textureInput = om.MObject()
    weightTextureInput = om.MObject()
    statusPreviewInput = om.MObject()
    annoyingTinyOutput = om.MObject()
    annoyingBaseOutput = om.MObject()


    callbackArray = om.MCallbackIdArray()
    permissionStatus = True

    def __init__(self):
        """Initialize class
        Mainly initializes the MFn classes to be reused.

        Attributes
        ---------
        tinyMatrixList : list
             List used as a history to check which tiny models have been changed.
        tinyMeshList : List
            List used as a history to check which tiny mesh have been changed.
        ovc : OcclusionVC()
            Class used for calculate occlusion.
        mfnAttr : MFnAttribute()
            Auxiliary class for internal use.
        mfnDagNode : MFnDagNode()
            Auxiliary class for internal use.
        mfnDependencyNode : MFnDependencyNode()
            Auxiliary class for internal use.
        mdgMode : MDgModifier()
            Auxiliary class for internal use.
        mdagMod : MDagModifier()
            Auxiliary class for internal use.

        """
        ompx.MPxObjectSet.__init__(self)
        self.tinyMatrixList = []
        self.tinyMeshList = []
        self.ovc = ovc.OcclusionVC()
        # This class needs some MFn functions and MDGModifier to make an attribute connection.
        self.mfnAttr = om.MFnAttribute()
        self.mfnDagNode = om.MFnDagNode()
        self.mfnDependecyNode = om.MFnDependencyNode()
        self.mdgMod = om.MDGModifier()
        self.mdagMod = om.MDagModifier()


    def setExistWithoutInConnections(self, flag):
        """Override method.
        'This method specifies whether or not the node can exist without input connections.'

        """
        flag = True
        return ompx.MPxObjectSet.setExistWithoutInConnections(self, flag)


    def setExistWithoutOutConnections(self, flag):
        """Override method.
        'This method specifies whether or not the node can exist without output connections.'

        """
        flag = True
        return ompx.MPxObjectSet.setExistWithoutOutConnections(self, flag)


    def canBeDeleted(self, isSrcNode):
        """Override method:
        'A method that is called whenever a neighboring node is deleted, to check if this node should be deleted
        alongside or as a result of the neighboring node.'

        """
        isSrcNode = False
        return ompx.MPxObjectSet.canBeDeleted(self, isSrcNode)

    def postConstructor(self):
        """This function is executed after the node is created in Maya scene.

        It creates a callback that will only be executed when this node is deleted. The callback fetches the connected
        anchorLocator and deletes it.

        It also creates a Locator to serve as a kind of anchor for OcclusionSetNode. It is not the most elegant
        solution. For model transformations to propagate through OcclusionSetNode, it needs to connect to something.
        In this case, a Locator was chosen. There are two connections. One for each type of function (modifications
        to the tiny models and modifications to the baseMesh).

        This Locator will only be created when the user creates a new instance. There is an attribute that prevents it
        from recreating the locator erroneously during the process of opening and importing the saved file.

        """
        thisObject = OcclusionSetNode.thisMObject(self)
        # Create callback to delete anchoLocator
        callbackId = om.MNodeMessage.addNodeAboutToDeleteCallback(thisObject, cb_nodePreDelete)
        OcclusionSetNode.callbackArray.append(callbackId)

        # Add icon
        self.mfnDependecyNode.setObject(thisObject)
        self.mfnDependecyNode.setIcon('occlusionSetNode.png')
        if OcclusionSetNode.permissionStatus:
            # Get annoyingTinyOutput MPlug
            self.mfnDependecyNode.setObject(thisObject)
            OcclusionTinyOutput = self.mfnDependecyNode.findPlug(annoyingTinyOutputLong)

            # Get annoyingBasemeshOutput MPlug
            OcclusionBasemeshOutput = self.mfnDependecyNode.findPlug(annoyingBaseMeshOutputLong)

            # Get dnSetMembers MPlug
            tmpPlug = self.mfnDependecyNode.findPlug('dnSetMembers')
            OcclusionDnSetMembersInput = tmpPlug.elementByLogicalIndex(0)

            # Create Locator
            locatorTransform = self.mdagMod.createNode('locator')
            self.mdagMod.doIt()

            # Rename locator
            self.mfnDependecyNode.setObject(locatorTransform)
            self.mfnDependecyNode.setName('anchorOcclusionNode')

            # Get Locator.translateX MPlug
            locatorTranslateXInput = self.mfnDependecyNode.findPlug('translateX')
            # Get Locator.translateY MPlug
            locatorTranslateYInput = self.mfnDependecyNode.findPlug('translateY')
            # Get Locator.message MPlug
            locatorMessageOutput = self.mfnDependecyNode.findPlug('message')

            # Set LocatorTransform attributes
            hiddenOutlinerAttr = self.mfnDependecyNode.findPlug('hiddenInOutliner')
            hiddenOutlinerAttr.setBool(True)
            templateAttr = self.mfnDependecyNode.findPlug('template')
            templateAttr.setBool(True)

            # Get LocatorShape
            self.mfnDagNode.setObject(locatorTransform)
            locatorShape = self.mfnDagNode.child(0)
            self.mfnDependecyNode.setObject(locatorShape)

            # Set LocatorShape attributes
            self.mfnDependecyNode.setName('anchorOcclusionNodeShape')
            hiddenOutlinerAttr = self.mfnDependecyNode.findPlug('hiddenInOutliner')
            hiddenOutlinerAttr.setBool(True)
            templateAttr = self.mfnDependecyNode.findPlug('template')
            templateAttr.setBool(True)
            localScaleX = self.mfnDependecyNode.findPlug('localScaleX')
            localScaleX.setFloat(0)
            localScaleY = self.mfnDependecyNode.findPlug('localScaleY')
            localScaleY.setFloat(0)
            localScaleZ = self.mfnDependecyNode.findPlug('localScaleZ')
            localScaleZ.setFloat(0)

            # Make connection between OcclusionNode to Locator
            self.mdgMod.connect(OcclusionBasemeshOutput, locatorTranslateXInput)
            self.mdgMod.connect(OcclusionTinyOutput, locatorTranslateYInput)
            self.mdgMod.connect(locatorMessageOutput, OcclusionDnSetMembersInput)
            self.mdgMod.doIt()

            # Pass this node (OcclusionSetNode) to class ovc.
            self.ovc.setObjectSet(thisObject)


    def connectionMade(self, plug, otherPlug, asSrc):
        """Override method:
        'This method gets called when connections are made to attributes of this node.'

        """

        # Get destination attribute name
        self.mfnAttr.setObject(plug.attribute())
        plugAttrName = self.mfnAttr.name()

        # Get MObjects from plug and otherPlug
        sourceNode = otherPlug.node()  # transform
        destNode = plug.node()  # this node
        # Get child node
        childNode = None
        if sourceNode.hasFn(om.MFn.kTransform):
            self.mfnDagNode.setObject(sourceNode)
            if self.mfnDagNode.childCount() > 0:
                childNode = self.mfnDagNode.child(0)
            else:
                childNode = None
        else:
            childNode = None

        ################################################################################
        # If its a "dagSetMembers" connection and child is a kMesh type
        # Automatically connect transform.worldMatrix to occlusionSetNode.tinyWorldMatrix using same logicalIndexPlug
        ################################################################################
        if plugAttrName == 'dagSetMembers' and childNode.apiType() == om.MFn.kMesh:
            # Get index from plug
            logicalIndexPlug = plug.logicalIndex()

            # Get MPlug of source and destination nodes (MPlug from arrays!!)
            self.mfnDependecyNode.setObject(sourceNode)
            tmpPlug = self.mfnDependecyNode.findPlug('worldMatrix')
            sourceWorldMatrixMPlug = tmpPlug.elementByLogicalIndex(0)

            self.mfnDependecyNode.setObject(childNode)
            sourceOutMeshMPlug = self.mfnDependecyNode.findPlug('outMesh')

            self.mfnDependecyNode.setObject(destNode)
            tmpPlug = self.mfnDependecyNode.findPlug(tinyWorldMatrixLong)
            destWorldMatrixMPlug = tmpPlug.elementByLogicalIndex(logicalIndexPlug)

            tmpPlug = self.mfnDependecyNode.findPlug(tinyMeshLong)
            destTinyMeshInputMPlug = tmpPlug.elementByLogicalIndex(logicalIndexPlug)


            # Make attribute connections using mdgMod.
            if OcclusionSetNode.permissionStatus:
                try:
                    self.mdgMod.connect(sourceWorldMatrixMPlug, destWorldMatrixMPlug)
                    self.mdgMod.doIt()
                    self.mdgMod.connect(sourceOutMeshMPlug, destTinyMeshInputMPlug)
                    self.mdgMod.doIt()
                except:
                    pass

            # Create colorSet to childNode

            self.ovc.createColorSet(childNode)


        ################################################################################
        # If its a "baseMesh" connection
        # Automatically connect mesh.outMesh to occlusionSetNode.baseMeshInput
        ################################################################################
        if plugAttrName == baseMeshLong:
            # Get parent(kTransform) of sourceNode(kMesh)
            self.mfnDagNode.setObject(sourceNode)
            if self.mfnDagNode.parentCount() > 0:
                # Get worldMatrix MPlug from sourceNodeParent
                sourceNodeParent = self.mfnDagNode.parent(0)
                self.mfnDependecyNode.setObject(sourceNodeParent)
                tmpPlug = self.mfnDependecyNode.findPlug('worldMatrix')
                sourceNodeParentMPlug = tmpPlug.elementByLogicalIndex(0)

                # Get baseMeshMatrixLong MPlug
                self.mfnDependecyNode.setObject(destNode)
                destWorldMatrixMPlug = self.mfnDependecyNode.findPlug(baseMeshMatrixLong)

                # Make attribute connections using mdgMod.
                if OcclusionSetNode.permissionStatus:
                    self.mdgMod.connect(sourceNodeParentMPlug, destWorldMatrixMPlug)
                    self.mdgMod.doIt()
                # Send MObjec of baseMesh to ovc
                self.ovc.setBaseModel(sourceNodeParent)

        return ompx.MPxObjectSet.connectionMade(self, plug, otherPlug, asSrc)


    def connectionBroken(self, plug, otherPlug, asSrc):
        """Override method:
        'This method gets called when connections are broken with attributes of this node.'

        """

        # Get destination attribute name
        self.mfnAttr.setObject(plug.attribute())
        plugAttrName = self.mfnAttr.name()

        # Get MObjects from plug and otherPlug
        otherNode = otherPlug.node()  # transform
        thisNode = plug.node()  # this node

        # Get child node
        childNode = None
        if otherNode.hasFn(om.MFn.kTransform):
            self.mfnDagNode.setObject(otherNode)
            if self.mfnDagNode.childCount() > 0:
                childNode = self.mfnDagNode.child(0)
            else:
                childNode = None
        else:
            childNode = None

        ################################################################################
        # If its a "dagSetMembers" connection and child is a kMesh type
        # Automatically connect transform.worldMatrix to occlusionSetNode.tinyWorldMatrix using same logicalIndexPlug
        ################################################################################
        if plugAttrName == 'dagSetMembers':
            # Get index from plug
            logicalIndexPlug = plug.logicalIndex()
            try:
                # Get MPlug of source and destination nodes (MPlug from arays!!)
                self.mfnDependecyNode.setObject(otherNode)
                tmpPlug = self.mfnDependecyNode.findPlug('worldMatrix')
                sourceWorldMatrixMPlug = tmpPlug.elementByLogicalIndex(0)

                self.mfnDependecyNode.setObject(childNode)
                sourceOutMeshMPlug = self.mfnDependecyNode.findPlug('outMesh')

                self.mfnDependecyNode.setObject(thisNode)
                tmpPlug = self.mfnDependecyNode.findPlug(tinyWorldMatrixLong)
                destWorldMatrixMPlug = tmpPlug.elementByLogicalIndex(logicalIndexPlug)

                tmpPlug = self.mfnDependecyNode.findPlug(tinyMeshLong)
                destTinyMeshInputMPlug = tmpPlug.elementByLogicalIndex(logicalIndexPlug)

                # Make attribute disconnect using mdgMod.
                if OcclusionSetNode.permissionStatus:
                    self.mdgMod.disconnect(sourceWorldMatrixMPlug, destWorldMatrixMPlug)
                    self.mdgMod.doIt()

                    self.mdgMod.disconnect(sourceOutMeshMPlug, destTinyMeshInputMPlug)
                    self.mdgMod.doIt()
            except:
                pass

        ################################################################################
        # If its a "baseMesh" connection and child is a kMesh type
        # Automatically connect transform.worldMatrix to occlusionSetNode.tinyWorldMatrix using same logicalIndexPlug
        ################################################################################
        if plugAttrName == baseMeshLong:
            try:
                # Get parent(kTransform) of sourceNode(kMesh)
                self.mfnDagNode.setObject(otherNode)
                if self.mfnDagNode.parentCount() > 0:
                    # Get worldMatrix MPlug from sourceNodeParent
                    sourceNodeParent = self.mfnDagNode.parent(0)
                    self.mfnDependecyNode.setObject(sourceNodeParent)
                    tmpPlug = self.mfnDependecyNode.findPlug('worldMatrix')
                    sourceNodeParentMPlug = tmpPlug.elementByLogicalIndex(0)

                    # Get baseMeshMatrixLong MPlug
                    self.mfnDependecyNode.setObject(thisNode)
                    destWorldMatrixMPlug = self.mfnDependecyNode.findPlug(baseMeshMatrixLong)

                    # Make attribute disconnections using mdgMod.
                    if OcclusionSetNode.permissionStatus:
                        self.mdgMod.disconnect(sourceNodeParentMPlug, destWorldMatrixMPlug)
                        self.mdgMod.doIt()
            except:
                pass

        return ompx.MPxObjectSet.connectionBroken(self, plug, otherPlug, asSrc)


    def compute(self, plug, dataBlock):
        """Override method.
        This function is executed when some connected models are altered.

        """
        if plug == OcclusionSetNode.annoyingTinyOutput or plug == OcclusionSetNode.annoyingBaseOutput:
            thisObjec = OcclusionSetNode.thisMObject(self)

            # Get tinyMatrixIn values
            wmatrixInHandle = dataBlock.inputArrayValue(OcclusionSetNode.tinyMatrixIn)
            totalTinyMatrixIn = wmatrixInHandle.elementCount()
            # Create Status for tinyModels connected
            tinyStatus = True if totalTinyMatrixIn > 0 else False

            # Get tinyMeshIn
            tinyMeshInHandle = dataBlock.inputArrayValue(OcclusionSetNode.tinyMeshIn)

            # Get DagSetMembers handler from tinyModels
            self.mfnDependecyNode.setObject(thisObjec)
            dagSetMembersMPlug = self.mfnDependecyNode.findPlug('dagSetMembers')

            # Get baseMeshInput
            baseMeshHandle = dataBlock.inputValue(OcclusionSetNode.baseMeshInput)
            baseMesh = baseMeshHandle.asMeshTransformed()

            # Get baseMeshMatrixInput
            baseMeshMatrixHandle = dataBlock.inputValue(OcclusionSetNode.baseMeshMatrixInput)
            baseMeshMatrix = baseMeshMatrixHandle.asMatrix()

            # Get status for baseMesh
            baseMeshStatus = False if baseMesh.isNull() else True

            # Get statusPreviewInput
            statusPreviewHandle = dataBlock.inputValue(OcclusionSetNode.statusPreviewInput)
            statusPreview = statusPreviewHandle.asBool()

            # Default value to weightTexture
            weightTextureValue = 0.0

            # Execute only if have tinyModels and baseMesh connecetd.
            if tinyStatus and baseMeshStatus:
                if plug == OcclusionSetNode.annoyingTinyOutput:

                    ###########################################
                    # Verify if have valid texture (procedural 3D)
                    # Get textureInput
                    textureSourceAttributeName = None
                    textureMPlug = self.mfnDependecyNode.findPlug(textureLong)

                    weightTextureValue = 0.0
                    if textureMPlug.isConnected():
                        textureSourceMPlug = textureMPlug.source()
                        textureSourceNode = textureSourceMPlug.node()
                        if textureSourceNode.hasFn(om.MFn.kTexture3d):
                            textureSourceAttributeName = textureSourceMPlug.info()

                            # Get weight texture value
                            weightTextureHandle = dataBlock.inputValue(OcclusionSetNode.weightTextureInput)
                            weightTextureValue = weightTextureHandle.asFloat()

                    tmpMatrixList = self.tinyMatrixList
                    tmpMeshList = self.tinyMeshList
                    self.tinyMatrixList = []
                    self.tinyMeshList = []
                    i = 0
                    while i < totalTinyMatrixIn:
                        wmatrixInHandle.jumpToArrayElement(i)
                        matrix = om.MTransformationMatrix(wmatrixInHandle.inputValue().asMatrix()).asMatrix()

                        tinyMeshInHandle.jumpToArrayElement(i)
                        mesh = tinyMeshInHandle.inputValue().asMesh()
                        if i < len(tmpMatrixList) and (tmpMatrixList[i] != matrix or tmpMeshList[i] != mesh):
                            # Get transform from dagSetMembers
                            plugArray = dagSetMembersMPlug.elementByLogicalIndex(i)
                            sourceMPlug = plugArray.source()
                            self.ovc.setTinyModel(sourceMPlug.node())
                            self.ovc.runOcclusion(textureSourceAttributeName, weightTextureValue, statusPreview)

                        self.tinyMatrixList.append(matrix)
                        self.tinyMeshList.append(mesh)
                        i += 1

                    ###########################################

                    annoyingOutputHandler = dataBlock.outputValue(OcclusionSetNode.annoyingTinyOutput)

                    annoyingOutputHandler.setInt(0)

                    dataBlock.setClean(plug)

                elif plug == OcclusionSetNode.annoyingBaseOutput:
                    ###########################################
                    # Get radiusValue and send to ovc class
                    radiusHandle = dataBlock.inputValue(OcclusionSetNode.radiusValueInput)
                    radiusInput = radiusHandle.asFloat()
                    self.ovc.setOcclusionRadius(radiusInput)

                    # Get smoothValue and send to ovc class
                    smoothHandle = dataBlock.inputValue(OcclusionSetNode.smoothValueInput)
                    smoothInput = smoothHandle.asFloat()
                    self.ovc.setOcclusionSmooth(smoothInput)

                    # Verify if have valid texture (procedural 3D)
                    # Get textureInput
                    textureSourceAttributeName = None
                    textureMPlug = self.mfnDependecyNode.findPlug(textureLong)

                    weightTextureValue = 0.0
                    if textureMPlug.isConnected():
                        textureSourceMPlug = textureMPlug.source()
                        textureSourceNode = textureSourceMPlug.node()
                        if textureSourceNode.hasFn(om.MFn.kTexture3d):
                            textureSourceAttributeName = textureSourceMPlug.info()

                            # Get weight texture value
                            weightTextureHandle = dataBlock.inputValue(OcclusionSetNode.weightTextureInput)
                            weightTextureValue = weightTextureHandle.asFloat()

                    # Star occlusion process
                    self.ovc.runOcclusionFromSet(textureSourceAttributeName, weightTextureValue, statusPreview)
                    ###########################################

                    annoyingBasemeshOutputHandler = dataBlock.outputValue(OcclusionSetNode.annoyingBaseOutput)

                    annoyingBasemeshOutputHandler.setInt(0)
                    dataBlock.setClean(plug)
                # Force refresh vertexColor. Thanks viewport 2.0
                om.MGlobal.executeCommandOnIdle('string $nodeList[];$nodeList = `ls -type "polyColorPerVertex"`;if (size($nodeList) > 0){dgdirty $nodeList;}')

        return om.kUnknownParameter


def enablePermissionStatus(*args, **kwargs):
    """ Function to enable permissionStatus.
    The permissionStatus define if the node will be processed or not.

    """
    OcclusionSetNode.permissionStatus = True


def disablePermissionStatus(*args, **kwargs):
    """ Function to disable permissionStatus
    The permissionStatus define if the node will be processed or not.

    """
    OcclusionSetNode.permissionStatus = False


def occlNodeCreator():
    """The function is responsible for creating the node during initialization.

    Return:
        Type of pointer.

    """
    return ompx.asMPxPtr(OcclusionSetNode())


def occlNodeInitializer():
    """This function creates and connects all attributes/plugs of the node.
    Include a definition of types for each attribute.

    """
    #############################################
    # Define tinyWorldMatrix attribute (input)
    matAttr = om.MFnMatrixAttribute()
    #   Here initialize using longName, shortName, and attribute type
    OcclusionSetNode.tinyMatrixIn = matAttr.create(tinyWorldMatrixLong,
                                                   tinyWorldMatrixShort,
                                                   om.MFnMatrixAttribute.kDouble)
    #   Its possible to define the behavior of this attribute
    matAttr.setArray(True)
    matAttr.setIndexMatters(False)
    matAttr.setStorable(False)
    matAttr.setHidden(True)

    #############################################
    # Define tinyMesh attribute (input)
    tAttr = om.MFnTypedAttribute()
    OcclusionSetNode.tinyMeshIn = tAttr.create(tinyMeshLong,
                                               tinyMeshShort,
                                               om.MFnData.kMesh)
    tAttr.setArray(True)
    tAttr.setIndexMatters(False)
    tAttr.setStorable(False)
    tAttr.setHidden(True)

    #############################################
    # Define baseMesh attribute (input)
    tAttr = om.MFnTypedAttribute()
    OcclusionSetNode.baseMeshInput = tAttr.create(baseMeshLong,
                                                  baseMeshShort,
                                                  om.MFnData.kMesh)
    tAttr.setHidden(False)

    #############################################
    # Define baseMeshMatrix attribute (input)
    matAttr = om.MFnMatrixAttribute()
    OcclusionSetNode.baseMeshMatrixInput = matAttr.create(baseMeshMatrixLong,
                                                          baseMeshMatrixShort,
                                                          om.MFnMatrixAttribute.kDouble)
    matAttr.setStorable(False)

    #############################################
    # Define radiusValue attribute (input)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.radiusValueInput = nAttr.create(radiusValueLong,
                                                     radiusValueShort,
                                                     om.MFnNumericData.kFloat, 1.0)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    nAttr.setReadable(True)
    nAttr.setWritable(True)
    nAttr.setMin(0.0)

    #############################################
    # Define smoothValue attribute (input)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.smoothValueInput = nAttr.create(smoothValueLong,
                                                     smoothValueShort,
                                                     om.MFnNumericData.kFloat, 0.5)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    nAttr.setReadable(True)
    nAttr.setWritable(True)
    nAttr.setMin(0.0)

    #############################################
    # Define texture attribute (input)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.textureInput = nAttr.createColor(textureLong,
                                                      textureShort)
    nAttr.setStorable(True)
    nAttr.setReadable(True)
    nAttr.setWritable(True)
    nAttr.setKeyable(True)

    #############################################
    # Define weight texture attribute (input)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.weightTextureInput = nAttr.create(weightTextureLong,
                                                       weightTextureShort,
                                                       om.MFnNumericData.kFloat, 0.0)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    nAttr.setReadable(True)
    nAttr.setWritable(True)

    #############################################
    # Define status Preview attribute (output)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.statusPreviewInput = nAttr.create(statusPreviewLong,
                                                       statusPreviewShort,
                                                       om.MFnNumericData.kBoolean, True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    nAttr.setReadable(True)
    nAttr.setWritable(True)

    #############################################
    # Define annoying tiny attribute (output)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.annoyingTinyOutput = nAttr.create(annoyingTinyOutputLong,
                                                       annoyingTinyOutputShort,
                                                       om.MFnNumericData.kInt)
    nAttr.setWritable(False)
    nAttr.setReadable(True)
    nAttr.setStorable(False)


    #############################################
    # Define annoying baseMesh attribute (output)
    nAttr = om.MFnNumericAttribute()
    OcclusionSetNode.annoyingBaseOutput = nAttr.create(annoyingBaseMeshOutputLong,
                                                       annoyingBaseMeshOutputShort,
                                                       om.MFnNumericData.kInt)
    nAttr.setWritable(False)
    nAttr.setReadable(True)
    nAttr.setStorable(False)


    #############################################
    # Connect all attributes to node
    OcclusionSetNode.addAttribute(OcclusionSetNode.tinyMatrixIn)
    OcclusionSetNode.addAttribute(OcclusionSetNode.tinyMeshIn)
    OcclusionSetNode.addAttribute(OcclusionSetNode.baseMeshInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.baseMeshMatrixInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.radiusValueInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.smoothValueInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.textureInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.weightTextureInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.statusPreviewInput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.annoyingTinyOutput)
    OcclusionSetNode.addAttribute(OcclusionSetNode.annoyingBaseOutput)

    #############################################
    # Define relationship
    OcclusionSetNode.attributeAffects(OcclusionSetNode.tinyMatrixIn, OcclusionSetNode.annoyingTinyOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.tinyMeshIn, OcclusionSetNode.annoyingTinyOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.baseMeshInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.baseMeshMatrixInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.radiusValueInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.smoothValueInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.textureInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.textureInput, OcclusionSetNode.annoyingTinyOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.weightTextureInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.weightTextureInput, OcclusionSetNode.annoyingTinyOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.statusPreviewInput, OcclusionSetNode.annoyingBaseOutput)
    OcclusionSetNode.attributeAffects(OcclusionSetNode.statusPreviewInput, OcclusionSetNode.annoyingTinyOutput)

def cb_nodePreDelete(*args, **kwargs):
    """A callback function that deletes anchorPoint when OcclusionSetNode is deleted.

    """
    # Get OcclusionNode from args
    OccNode = args[0]

    # Get annoyingTinyOuput and annoyingBaseMeshOutput MPlug
    mfnDep = om.MFnDependencyNode(OccNode)
    mfnDag = om.MFnDagNode()
    OccTinyMPlug = mfnDep.findPlug(annoyingTinyOutputLong)
    OccBaseMeshMPlug = mfnDep.findPlug(annoyingBaseMeshOutputLong)

    path01 = None
    path02 = None

    # Get MPlug from destination.
    mplugArray = om.MPlugArray()
    OccTinyMPlug.destinations(mplugArray)
    if mplugArray.length() > 0:
        tinyMPlugDestination = mplugArray[0]
        # Get MObject from destination MPlug.
        destinationNode01 = tinyMPlugDestination.node()
        mfnDag.setObject(destinationNode01)
        # Get path to destination node
        path01 = mfnDag.fullPathName()

    mplugArray = om.MPlugArray()
    OccBaseMeshMPlug.destinations(mplugArray)
    if mplugArray.length() > 0:
        baseMeshMPlugDestination = mplugArray[0]
        # Get MObject from destination MPlug.
        destinationNode02 = tinyMPlugDestination.node()
        mfnDag.setObject(destinationNode02)
        # Get path to destination node
        path02 = mfnDag.fullPathName()

    try:
        if cmds.objExists(path01):
            cmds.delete(path01)
        if cmds.objExists(path02):
            cmds.delete(path02)
    except:
        pass


def initializePlugin(mobj):
    """This function declares and initializes plug-in in memory of Maya.
    Also creates callbacks to pause node processing in save, open, import stage.

    """
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.registerNode(kPluginNodeTypeName,
                             occSetNodeId,
                             occlNodeCreator,
                             occlNodeInitializer,
                             ompx.MPxNode.kObjectSet)

        # Create all callbacks
        id = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterOpen, enablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)
        id = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterSave, enablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)
        id = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterImport, enablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)

        id = om.MSceneMessage.addCallback(om.MSceneMessage.kBeforeOpen, disablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)
        id = om.MSceneMessage.addCallback(om.MSceneMessage.kBeforeSave, disablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)
        id = om.MSceneMessage.addCallback(om.MSceneMessage.kBeforeImport, disablePermissionStatus)
        OcclusionSetNode.callbackArray.append(id)
    except:
        sys.stderr.write("Failed to register node: {}".format(kPluginNodeTypeName))
        raise


def uninitializePlugin(mobj):
    """This function delete plugin in memory of Maya.
    Also, delete all callbacks.

    """
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.deregisterNode(occSetNodeId)
        # Delete all callbacks
        om.MSceneMessage.removeCallbacks(OcclusionSetNode.callbackArray)
    except:
        sys.stderr.write("Failed to deregister node: {}".format(kPluginNodeTypeName))
        raise
