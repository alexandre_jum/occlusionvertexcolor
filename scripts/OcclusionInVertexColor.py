import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaRender as omr


class ClosestPoint():
    """
    The ClosestPoint object calculates the closest point on the mesh from a coordinate passed.

    Parameters
    ----------

    Attributes
    ----------
    baseModelShape :   MObject (kMesh)
    baseModelDagPath : MDagPath
    baseModelMMatrix : MMatrix
    intersector :      MMeshIntersector

    """

    def __init__(self):
        self.baseModelShape = None
        self.baseModelDagPath = om.MDagPath()
        self.baseModelMMatrix = om.MMatrix()
        self.intersector = om.MMeshIntersector()

    def setBaseModelShape(self, baseModelShape):
        """Set baseModelShape for calculation

        If baseModelShape is valid MObject(kMesh), get your MDagPath and your inclusiveMatrix.
        At the end, initialize MMeshIntersector.

        Parameters
        ----------
        baseModelShape : MObject(kMesh)

        """
        # Verify if baseModelShape is a MObject and kMesh
        if type(baseModelShape) == om.MObject and baseModelShape.apiType() == om.MFn.kMesh:
            self.baseModelShape = baseModelShape

            # Get DagPath to baseModelShape
            om.MDagPath.getAPathTo(self.baseModelShape, self.baseModelDagPath)

            # Get inclusiveMatrix of baseModeShape using MDagPath
            self.baseModelMMatrix = self.baseModelDagPath.inclusiveMatrix()

            # Initialize MMeshIntersector
            self.intersector.create(self.baseModelShape, self.baseModelMMatrix)
        else:
            cmds.error("baseModelShape: wrong type. Need be a MObject(kMesh)")

    def getClosestPoint(self, pos):
        """Get closest point on mesh.

        Calculate the closest point on the mesh from pos coordinate.

        Parameters
        ----------
        pos : MPoint


        Returns
        -------
        point : MPoint
            Coordinate in worldSpace
            None if received the wrong type

        """
        if type(pos) == om.MPoint:
            if self.intersector.isCreated():
                # Create MPointOnMesh to receive the result from MMeshIntersector
                pointOnMesh = om.MPointOnMesh()

                # run intersector using point coordinate
                self.intersector.getClosestPoint(pos, pointOnMesh)

                # Get local closest point. Need be a MPoint
                point = om.MPoint(pointOnMesh.getPoint())
                point = point * self.baseModelMMatrix

                return point

        else:
            cmds.error('pos: wront type. Need be a MPoint')
            return None


class OcclusionVC():
    """
    The OcclusionVC object calculate occlusion from one model to other.

    Parameter
    ---------

    Attributes
    ----------
    baseModelShape :    MObject (kMesh)
    tinyModel :         MObject (kTransform)
    tinyModelShape :    MObject (kMesh)
    tinyModelDagPath :  MDagPath
    selList :           MSelectionList
    closestPoint :      ClosestPoint
    mfnSet:             MFnSet
    mfnDependencyNode:  MFnDependencyNode
    __radius :          float
    __smooth :          float
    __mfnMesh :         MFnMesh
    __colorSetName:     string

    """

    def __init__(self):
        self.baseModelShape = None
        self.tinyModelShape = None
        self.tinyModelDagPath = om.MDagPath()
        self.setObj = om.MObject()
        self.textureAttibuteName = None
        self.weightTexture = 0.0
        self.statusPreview = True
        self.selList = om.MSelectionList()
        self.closestPoint = ClosestPoint()
        self.mfnSet = om.MFnSet()
        self.mfnDependencyNode = om.MFnDependencyNode()
        self.__radius = 0.35
        self.__smooth = 0.2
        self.__mfnMesh = om.MFnMesh()
        self.__colorSetName = 'occlusionVC'

    def setOcclusionRadius(self, value):
        """Set occlusion radius value.

        Parameter
        ---------
        value : float

        """
        self.__radius = value

    def setOcclusionSmooth(self, value):
        """Set Occlusion smooth value

        Parameter
        ---------
        value : float
            Set occlusion ramp.
        """
        if value == 0:
            value = 0.00001
        self.__smooth = value

    def setBaseModel(self, baseModelName):
        """Set base model to calculate Occlusion

        Parameter
        ---------
        baseModelName : string or MObject
            Received model name or MObject. Need be the name or MObject of the transform model.

        """
        baseModel = om.MObject()
        if type(baseModelName) == str:
            self.selList.clear()
            self.selList.add(baseModelName)
            self.selList.getDependNode(0, baseModel)
        elif type(baseModelName) == om.MObject:
            baseModel = baseModelName

        # First verify if baseModel is a MObject (kTransform)
        if baseModel.apiType() == om.MFn.kTransform:
            # Get MDagPath to baseModel. Its necessary to get your child in hierarchy
            tmpDagPath = om.MDagPath()
            om.MDagPath.getAPathTo(baseModel, tmpDagPath)
            child = tmpDagPath.child(0)

            # Verify if child is a kMesh type.
            if child.apiType() == om.MFn.kMesh:
                self.baseModelShape = child

            else:
                self.baseModelShape = None
                cmds.error('baseModel: wrong Type. Need be a model')
        else:
            self.baseModelShape = None
            cmds.error('baseModel: wrong Type. Need be a model')

    def setTinyModel(self, tinyModelName):
        """Set tiny model to calculate Occlusion

        Parameter
        ----------
        tinyModelName : string or MObject
            Received model name or your MObject. Need be the name or MObject of the transform model.

        """
        tinyModel = om.MObject()
        if type(tinyModelName) == str:
            self.selList.clear()
            self.selList.add(tinyModelName)
            self.selList.getDependNode(0, tinyModel)
        elif type(tinyModel) == om.MObject:
            tinyModel = tinyModelName

        # First verify if tinyModel is a MObject (kTransform)
        if type(tinyModel) == om.MObject and tinyModel.apiType() == om.MFn.kTransform:
            # Get MDagPath to tinyModel. Its necessary to get your child in hierarchy
            self.tinyModelDagPath = om.MDagPath()
            om.MDagPath.getAPathTo(tinyModel, self.tinyModelDagPath)
            child = self.tinyModelDagPath.child(0)

            # Verify if child is a kMesh type.
            if child.apiType() == om.MFn.kMesh:
                self.tinyModelShape = child
            else:
                self.tinyModelShape = None
                self.tinyModelDagPath = om.MDagPath()
                cmds.error("tinyModel: wrong Type. Need be a model")
        else:
            self.tinyModelShape = None
            self.tinyModelDagPath = om.MDagPath()


    def setObjectSet(self, setName):
        """Set objectSet that contains all the tiny models.

        Parameter
        ---------
        setName : string
            Name of objectSet

        """
        tmpObj = om.MObject()
        if type(setName) == str:
            self.selList.clear()
            self.selList.add(setName)
            self.selList.getDependNode(0, tmpObj)

        elif type(setName) == om.MObject:
            tmpObj = setName

        # Verify is setName is a objectSet
        if tmpObj.apiType() == om.MFn.kPluginObjectSet:
            self.setObj = tmpObj
        else:
            self.setObj = om.MObject()

    def createColorSet(self, modelShape):
        """ Create a new colorSet to tinyMesh
        """
        self.__mfnMesh.setObject(modelShape)
        # Verify if exist a colorSet for Occlusion
        colorSetNames = []
        self.__mfnMesh.getColorSetNames(colorSetNames)
        if not self.__colorSetName in colorSetNames:
            name = self.__mfnMesh.createColorSetWithName(self.__colorSetName)
            print 'Create ColorSet: ', name

    def __run(self, tinyModelDagPath):
        """Both functions (runOcclusion / runOcclusionFromSet) invoque this function internally.
        Iterate over all vertices of model from tinyModelDagPath to calculate the distance with the baseModel.
        After iteration:
        - enable display vertex color;
        - enable 'Export Vertex Color' for Arnold;
        - create or set colorSet for apply color in vertex;
        - apply color.

        Parameter
        ---------
        tinyModelDagPath : MDagPath
            MDagPath of tinyModel

        """
        # Verify if self.baseModelShape and self.tinyModelDagPath are valid
        if self.baseModelShape and tinyModelDagPath.isValid():
            # set self.baseModelShape in self.closestPoint object
            self.closestPoint.setBaseModelShape(self.baseModelShape)

            # Create Iterator (MItMeshVertex) to iterate all vertex of tinyModel
            vertexIt = om.MItMeshVertex(tinyModelDagPath)
            totalVertex = vertexIt.count()

            # Need MColorArray and MIntArray to colorize all vertex
            mColorArray = om.MColorArray()
            mIntArray = om.MIntArray()

            # Two MFloatPointArray to calculate texture 3D
            pointArray = om.MFloatPointArray()
            refPoints = om.MFloatPointArray()
            pointArray.setLength(totalVertex)
            refPoints.setLength(totalVertex)
            # Get luminance value from texture 3D
            resultColors = om.MFloatVectorArray()
            resultTransparencies = om.MFloatVectorArray()
            if self.textureAttibuteName:
                useShadowMaps = False
                reuseMaps = False
                cameraMatrix = om.MFloatMatrix()
                uCoords = None
                vCoords = None
                normals = None
                tangentUs = None
                tangentVs = None
                filterSizes = None


                # Its time to iterate!!!!

                # First iterate get all color values from texture 3D
                while (not vertexIt.isDone()):
                    vtIdx = vertexIt.index()
                    vtPos = vertexIt.position(om.MSpace.kWorld)

                    # Append vertex position to calculate texture 3D
                    vtFloatPos = om.MFloatPoint(vtPos)
                    index = vertexIt.index()
                    pointArray.set(vtFloatPos, index)
                    refPoints.set(vtFloatPos, index)

                    vertexIt.next()

                # Get texture 3d color values from pointArray/refPoints
                omr.MRenderUtil.sampleShadingNetwork(self.textureAttibuteName,
                                                     totalVertex,
                                                     useShadowMaps,
                                                     reuseMaps,
                                                     cameraMatrix,
                                                     pointArray,
                                                     uCoords,
                                                     vCoords,
                                                     normals,
                                                     refPoints,
                                                     tangentUs,
                                                     tangentVs,
                                                     filterSizes,
                                                     resultColors,
                                                     resultTransparencies)

            # Second iterate calculate distance between each vertex with baseMesh
            vertexIt.reset()
            while (not vertexIt.isDone()):
                vtIdx = vertexIt.index()
                vtPos = vertexIt.position(om.MSpace.kWorld)

                if resultColors.length()>0:
                    dist = self.__getDistance(vtPos, resultColors[vertexIt.index()])
                else:
                    dist = self.__getDistance(vtPos)

                # Calculate smootstep of occlusion based in radius value
                colorValue = self.__smoothstep(dist)

                # Append new color and vertex index
                mColorArray.append(om.MColor(colorValue, colorValue, colorValue))
                mIntArray.append(vtIdx)

                vertexIt.next()

            # Use MFnDependencyNode to get mesh attributes
            tinyMesh = tinyModelDagPath.child(0)
            self.mfnDependencyNode.setObject(tinyMesh)




            self.__mfnMesh.setObject(tinyModelDagPath)

            # Really?!?!? Thank you 2.0 preview window. Because of you, I am forced to disable DisplayColor,
            # process the vertices, and reactivate it.
            self.__mfnMesh.setDisplayColors(False)

            #   Set current colorSet
            self.__mfnMesh.setCurrentColorSetName(self.__colorSetName)

            # Apply all vertex color
            self.__mfnMesh.setVertexColors(mColorArray, mIntArray)

            # Enable 'Export Vertex Color' for Arnold
            if self.mfnDependencyNode.hasAttribute('aiExportColors'):
                arnoldExpVtxColorMPlug = self.mfnDependencyNode.findPlug('aiExportColors')
                arnoldExpVtxColorMPlug.setBool(True)
            # If statusPreview enable displayColors and set mesh color channel to None
            if self.mfnDependencyNode.hasAttribute('displayColorChannel'):
                tmpPlug = self.mfnDependencyNode.findPlug('displayColorChannel')
                if self.statusPreview:
                    tmpPlug.setString('None')
                else:
                    tmpPlug.setString('Ambient+Diffuse')


            # Reenable DisplayColors (Thanks viewport 2.0)
            self.__mfnMesh.setDisplayColors(self.statusPreview)


    def runOcclusion(self, textureAttributeName = None, weightTexture = 0.0, statusPreview = True):
        """Start occlusion calculation using individual tinyModel.

        Parameter
        ---------
        textureAttributeName : string
            Node name + channel. Ex. "volumeNoise.outputColor"
        weightTexture : float
            Value that control texture influence.
        """
        # Verify if self.baseModelShape and self.tinyModelDagPath are valid
        if self.baseModelShape and self.tinyModelDagPath.isValid():
            self.textureAttibuteName = textureAttributeName
            self.weightTexture = weightTexture
            self.statusPreview = statusPreview
            self.__run(self.tinyModelDagPath)


    def runOcclusionFromSet(self, textureAttributeName = None, weightTexture = 0.0, statusPreview = True):
        """Start occlusion calculation using all models in objectSet

        Parameter
        ---------
        textureAttributeName : string
            Node name + channel. Ex. "volumeNoise.outputColor"
        weightTexture : float
            Value that control texture influence.
        """
        # Verify if setObj and baseModelShape are valid
        if not self.setObj.isNull() and self.baseModelShape:
            self.textureAttibuteName = textureAttributeName
            self.weightTexture = weightTexture
            self.statusPreview = statusPreview
            # Get all models from objectSet
            self.mfnSet.setObject(self.setObj)
            modelSelList = om.MSelectionList()
            self.mfnSet.getMembers(modelSelList, True)

            # Create a iterator to modelSelList
            selIt = om.MItSelectionList(modelSelList)
            while not selIt.isDone():
                # To get models inside iterator (from modelSelList/objectSet) we need get as MObject and your MDagPath.
                # Using MObject we can know if is a kTransform type. MDagPath can access your child (probably a shape)
                # and verify if is a kMesh type.
                itemObj = om.MObject()
                selIt.getDependNode(itemObj)
                # Verify if is a kTransform type
                if itemObj.apiType() == om.MFn.kTransform:
                    # Get DagPath of itemObj
                    itemDagPath = om.MDagPath()
                    om.MDagPath.getAPathTo(itemObj, itemDagPath)
                    # ... and now, your child. But only if have a child.
                    if itemDagPath.childCount() > 0:
                        child = om.MObject()
                        child = itemDagPath.child(0)
                        # If the child is a kMesh type, send to the calculation
                        if child.apiType() == om.MFn.kMesh:
                            self.__run(itemDagPath)

                selIt.next()

    def __getDistance(self, pos, textureColor = None):
        """Use ClosestPoint class to calculate distance between pos(MPoint) and baseModel

        Parameter
        ---------
        pos : MPoint
            Received coord from one vertex from tinyMehsh

        """
        # Get coords of closestPoint from pos to baseMesh
        baseModelPoint = self.closestPoint.getClosestPoint(pos)

        point = baseModelPoint - pos
        distance = point.length()

        if textureColor:
            noiseValue = (distance * textureColor.x) * self.weightTexture
            distance = distance + noiseValue

        return distance

    def __smoothstep(self, value):
        """This is a... smoothstep. So.. thats it.

        Parameter
        ---------
        value : float
            This value is a distance between vertex and baseMesh.
        """
        # Define range of smootstep
        minValue = self.__radius - self.__smooth
        maxValue = self.__radius + self.__smooth

        result = (value - minValue) / (maxValue - minValue)
        result = max(min(result, 1.0), 0.0)
        result = result * result * (3 - 2 * result)

        return result

    def changePreview(self, tinyMesh, status):
        self.mfnDependencyNode.setObject(tinyMesh)
        # Enable 'Export Vertex Color' for Arnold
        if self.mfnDependencyNode.hasAttribute('aiExportColors'):
            arnoldExpVtxColorMPlug = self.mfnDependencyNode.findPlug('aiExportColors')
            arnoldExpVtxColorMPlug.setBool(status)
